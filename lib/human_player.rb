class HumanPlayer
  attr_accessor :name, :mark, :board

  def initialize(name)
    @name = name
  end

  def get_move
    puts "Where do you want to move? Enter in the form of (y, x)."
    input = gets.chomp.split(",").map(&:to_i)
  end

  def display(board)
    @board = Board.new(board)
    puts "Here is the board:"
    (0..2).each do |y|
      (0..2).each do |x|
        print (board.empty?([y, x]) ? "." : board[y][x])
      end
      puts
    end
  end
end
