class Board
  attr_accessor :grid, :mark

  def self.new_grid
    Array.new(3){Array.new(3)}
  end

  def initialize(grid = Board.new_grid)
    @grid = grid
  end

  def place_mark(pos, mark)
    @grid[pos[0]][pos[1]] = mark
  end

  def empty?(pos)
    @grid[pos[0]][pos[1]].nil?
  end

  def [](pos)
    grid[pos[0]][pos[1]]
  end

  def []=(row, col, value)
    grid[row][col] = value
  end

  def winner
    matrix = [diagonals, rows, colums].flatten(1)
    matrix.each do |line|
      return :O if line == [:O, :O, :O]
      return :X if line == [:X, :X, :X]
    end
    nil
  end

  def over?
    @grid.flatten.compact.size == 9 || winner
  end

  def diagonals
    left_to_right = [[0, 0], [1, 1], [2, 2]]
    right_to_left = [[0, 2], [1, 1], [2, 0]]
    [left_to_right, right_to_left].map do |diag|
      diag.map{|y, x| @grid[y][x]}
    end
  end

  def rows
    [0, 1, 2].map do |y|
      [0, 1, 2].map {|x| @grid[y][x]}
    end
  end

  def colums
    [0, 1, 2].map do |x|
      [0, 1, 2].map {|y| @grid[y][x]}
    end
  end
end
