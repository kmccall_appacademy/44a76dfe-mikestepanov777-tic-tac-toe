class ComputerPlayer
attr_accessor :name, :board, :mark

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    moves = open_spots()
    moves.each do |move|
      return move if victorious?(move)
    end
    moves.sample
  end

  def open_spots
    open_spots = []
    (0..2).each do |y|
      (0..2).each do |x|
        open_spots << [y, x] if @board.empty?([y, x])
      end
    end
    open_spots
  end

  def victorious?(move)
    board[*move] = mark
    victory = board.winner
    board[*move] = nil
    victory
  end
end
