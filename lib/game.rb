require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board, :current_player, :player1, :player2

  def initialize(player1, player2)
    @player1 = player1
    @player2 = player2
    @player1.mark = :X
    @player2.mark = :O
    @board = Board.new
    @current_player = @player1
  end

  def board
    @board
  end

  def play
    until @board.over?
      @current_player.display(@board)
      play_turn
    end

    if victorious
      "#{victorious.name} has won!"
    else
      "Draw."
    end
  end

  def victorious
    return player1 if @board.winner == @player1.mark
    return player2 if @board.winner == @player2.mark
    nil
  end


  def play_turn
    @board.place_mark(@current_player.get_move, @current_player.mark)
    switch_players!
  end

  def switch_players!
    @current_player = (@current_player == @player1 ? @player2 : @player1)
  end

  def current_player
    @current_player
  end
end
